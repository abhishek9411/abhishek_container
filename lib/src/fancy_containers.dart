import 'package:flutter/material.dart';

class FancyContainer extends StatefulWidget {
  const FancyContainer({Key? key}) : super(key: key);

  @override
  _FancyContainerState createState() => _FancyContainerState();
}

class _FancyContainerState extends State<FancyContainer> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
